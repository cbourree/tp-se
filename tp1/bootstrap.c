/* Cortex M0 core interrupt handlers */
void Reset_Handler(void);
void NMI_Handler(void) __attribute__ ((weak, alias ("Dummy_Handler")));
void HardFault_Handler(void) __attribute__ ((weak, alias ("Dummy_Handler")));

void Dummy_Handler(void);
extern unsigned int _end_stack;

void *vector_table[] __attribute__ ((section(".vectors"))) = {
   &_end_stack, /* Initial SP value */ /* 0 */
   Reset_Handler,
   NMI_Handler,
   HardFault_Handler,
   0,
   0, /* 5 */
   0,
   /* Entry 7 (8th entry) must contain the 2’s complement of the check-sum
      of table entries 0 through 6. This causes the checksum of the first 8
      table entries to be 0 */
   (void *)0xDEADBEEF, /* Actually, this is done using an external tool. */
   0,
   /* [.....] voir le code du module GPIO-Demo pour la suite */
};

void Dummy_Handler(void) {
    while (1);
}

int main(void);
void Reset_Handler(void) {
	/* Our program entry ! */
	main();
}
