#include <stdint.h>
#define LPC_AHB_BASE  (0x50000000UL)
#define LPC_GPIO_0_BASE  (LPC_AHB_BASE + 0x00000)
#define LPC_GPIO_1_BASE  (LPC_AHB_BASE + 0x10000)
/* General Purpose Input/Output (GPIO) */
struct lpc_gpio
{
   volatile uint32_t mask;       /* 0x00 : Pin mask, affects data, out, set, clear and invert */
   volatile uint32_t in;         /* 0x04 : Port data Register (R/-) */
   volatile uint32_t out;        /* 0x08 : Port output Register (R/W) */
   volatile uint32_t set;        /* 0x0C : Port output set Register (-/W) */
   volatile uint32_t clear;      /* 0x10 : Port output clear Register (-/W) */
   volatile uint32_t toggle;     /* 0x14 : Port output invert Register (-/W) */
   uint32_t reserved[2];
   volatile uint32_t data_dir;   /* 0x20 : Data direction Register (R/W) */
   /* [.....] */
};
#define LPC_GPIO_0      ((struct lpc_gpio *) LPC_GPIO_0_BASE)
#define LPC_GPIO_1      ((struct lpc_gpio *) LPC_GPIO_1_BASE)

/* The status LED is on GPIO Port 1, pin 4 (PIO1_4) and Port 1, pin 5 (PIO1_5) */
#define LED_RED    5
#define LED_GREEN  4

#define nop() __asm volatile ("nop")

/*
 * Notre exemple simple pour le TP
 */
void system_init(void)
{
	/* System init ? */
}

void random_sleep(void)
{
	uint32_t i;
	for (i = 0; i < 4294967 ; i++) {
		nop();
	}
	return;	
}

int main(void)
{
	struct lpc_gpio* gpio1 = LPC_GPIO_1;
	uint32_t current_color = LED_GREEN;
   	/* Micro-controller init */
  	system_init();
   	/* Configure the Status Led pins */
   	gpio1->data_dir |= (1 << LED_GREEN) | (1 << LED_RED);
   	/* Turn Green Led ON */
   	gpio1->set = (1 << LED_GREEN);
   	gpio1->clear = (1 << LED_RED);
   	while (1) {
       		/* Change the led state */
       		/* Wait some time */
		/*if (current_color == LED_GREEN) {
			gpio1->set = (1 << LED_RED);
   			gpio1->clear = (1 << LED_GREEN);
			current_color = LED_RED;
		} else {
			gpio1->set = (1 << LED_GREEN);
   			gpio1->clear = (1 << LED_RED);
			current_color = LED_GREEN;
		}
		random_sleep();*/
		
   	}
	return 0;
}

