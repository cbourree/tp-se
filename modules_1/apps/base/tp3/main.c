/****************************************************************************
 *   apps/base/servomotor/main.c
 *
 * Servo-motor example
 *
 * Copyright 2016 Nathael Pajani <nathael.pajani@ed3l.fr>
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *************************************************************************** */

#include "neopixel.h"

#include "core/system.h"
#include "core/systick.h"
#include "core/pio.h"
#include "lib/stdio.h"
#include "drivers/serial.h"
#include "drivers/gpio.h"
#include "extdrv/status_led.h"
#include "drivers/adc.h"
#include "drivers/timers.h"


#define MODULE_VERSION   0x04
#define MODULE_NAME "GPIO Demo Module"


#define SELECTED_FREQ  FREQ_SEL_48MHz

/* Chose on of these depending on the signal you need:
 *  - inverted one (3), for use with a single transistor
 *  - non inverted (40 - 3), for use with two transistors (or none).
 */
#define DUTY_INVERTED 0
#if (DUTY_INVERTED == 1)
#define SERVO_MED_POS_DUTY_CYCLE  3
#else
#define SERVO_MED_POS_DUTY_CYCLE  (40 - 3)
#endif

#define LPC_TIMER_PIN_CONFIG   (LPC_IO_MODE_PULL_UP | LPC_IO_DIGITAL | LPC_IO_DRIVE_HIGHCURENT)

/***************************************************************************** */
/* Pins configuration */
/* pins blocks are passed to set_pins() for pins configuration.
 * Unused pin blocks can be removed safely with the corresponding set_pins() call
 * All pins blocks may be safelly merged in a single block for single set_pins() call..
 */
const struct pio_config common_pins[] = {
	/* UART 0 */
	{ LPC_UART0_RX_PIO_0_1,  LPC_IO_DIGITAL },
	{ LPC_UART0_TX_PIO_0_2,  LPC_IO_DIGITAL },
	/* UART 1 */
	{ LPC_UART1_RX_PIO_0_8, LPC_IO_DIGITAL },
	{ LPC_UART1_TX_PIO_0_9, LPC_IO_DIGITAL },
	/* TIMER_32B0 */
	{ LPC_TIMER_32B1_M0_PIO_0_23, LPC_TIMER_PIN_CONFIG },
	ARRAY_LAST_PIO,
};

const struct pio_config adc_pins[] = {
	{ LPC_ADC_AD0_PIO_0_30, LPC_IO_ANALOG },
	{ LPC_ADC_AD1_PIO_0_31, LPC_IO_ANALOG },
	{ LPC_ADC_AD2_PIO_1_0,  LPC_IO_ANALOG },
	{ LPC_ADC_AD3_PIO_1_1,  LPC_IO_ANALOG },
	{ LPC_ADC_AD4_PIO_1_2,  LPC_IO_ANALOG },
	{ LPC_ADC_AD5_PIO_1_3,  LPC_IO_ANALOG },
	ARRAY_LAST_PIO,
};

const struct pio status_led_green = LPC_GPIO_1_4;
const struct pio status_led_red = LPC_GPIO_1_5;

const struct pio sensor_echo = LPC_GPIO_0_11; /* Ultrasonic sensor signal */
const struct pio sensor_trig = LPC_GPIO_0_10; /* Ultrasonic sensor signal */


/***************************************************************************** */
static uint32_t servo_med_pos_cmd = 0;
static uint32_t servo_one_deg_step = 0;
static uint8_t timer = 0;
static uint8_t channel = 0;

int servo_config(uint8_t timer_num, uint8_t pwm_chan, uint8_t uart_num)
{
	uint32_t servo_command_period = 0;
	struct lpc_timer_pwm_config timer_conf = {
		.nb_channels = 1,
		.period_chan = 3,
	};

	if (timer_num > LPC_TIMER_32B1) {
		uprintf(uart_num, "Bad timer number\n");
		return -1;
	}
	if (pwm_chan >= 3) {
		uprintf(uart_num, "Bad channel number\n");
		return -1;
	}
	timer = timer_num;
	channel = pwm_chan;
	timer_conf.outputs[0] = pwm_chan;

	/* compute the period and median position for the servo command */
	/* We want 20ms (50Hz), timer counts at main clock frequency */
	servo_command_period = get_main_clock() / 50;
	/* servo_command_period is 20ms, we need 1.5ms, which is 3/40. */
	servo_med_pos_cmd = ((servo_command_period / 40) * SERVO_MED_POS_DUTY_CYCLE);
	servo_one_deg_step = ((servo_command_period / 41) / 48);
	timer_conf.match_values[0] = servo_med_pos_cmd;
	timer_conf.period = servo_command_period;

	timer_on(timer, 0, NULL);
	timer_pwm_config(timer, &timer_conf);
	timer_start(timer);

	uprintf(uart_num, "Servos configured (T%d : C%d), Period : %d, med_pos : %d\n",
						timer, channel, servo_command_period, servo_med_pos_cmd);

	return 0;
}

void set_servo(int servo_id, int angle, int uart_num)
{
	uint32_t pos = servo_med_pos_cmd;

	if (angle > 180) {
		angle = 180;
	}

	/* And compute the new match value for the angle */
	if (angle >= 90) {
		pos += ((angle - 90) * servo_one_deg_step);
	} else {
		pos -= ((90 - angle) * servo_one_deg_step);
	}
	timer_set_match(timer, servo_id, pos);
	uprintf(uart_num, "Servo(%d): %d (%d)\n", servo_id, angle, pos);
}

/***************************************************************************** */
void system_init()
{
	/* Stop the watchdog */
	startup_watchdog_disable(); /* Do it right now, before it gets a chance to break in */
	system_set_default_power_state();
	clock_config(SELECTED_FREQ);
	set_pins(common_pins);
	set_pins(adc_pins);
	gpio_on();
	status_led_config(&status_led_green, &status_led_red);
	/* System tick timer MUST be configured and running in order to use the sleeping
	 * functions */
	systick_timer_on(1); /* 1ms */
	/*------config ultrasonic---------*/
	system_brown_out_detection_config(0); /* No ADC used */

	systick_start();
	config_pin();
}

/* Define our fault handler. This one is not mandatory, the dummy fault handler
 * will be used when it's not overridden here.
 * Note : The default one does a simple infinite loop. If the watchdog is deactivated
 * the system will hang.
 */
void fault_info(const char* name, uint32_t len)
{
	uprintf(UART0, name);
	while (1);
}


static uint8_t send = 0;
static uint8_t cpt = 0;
static uint8_t cmd[20];

void strip_control(uint8_t c)
{
	cmd[cpt] = c;
	if (c == '.') {
		cpt = 0;
		send = 1;
	} else {
		cpt++;
	}
}

/* Note that clock cycles counter wraps every 89 seconds with system clock running at 48 MHz */
static volatile uint32_t pulse_start = 0;  /* Clock cycles counter upon echo start */
static volatile uint32_t pulse_end = 0;    /* Clock cycles counter upon echo end */
static volatile uint32_t pulse_duration = 0;
void pulse_feedback(uint32_t gpio) {
	static uint32_t pulse_state = 0;
	if (pulse_state == 0) {
		pulse_start = systick_get_clock_cycles();
		pulse_state = 1;
	} else {
		pulse_end = systick_get_clock_cycles();
		if (pulse_end > pulse_start) {
			pulse_duration = (pulse_end - pulse_start);
		} else {
			pulse_duration = (0xFFFFFFFF - pulse_start);
			pulse_duration += pulse_end;
		}
		pulse_state = 0;
	}
}

void send_ultrasonic_sensor_value_on_uart(void)
{
	uint32_t distance = 0; /* of ultrasonic sensor */

	/* Initiate distance mesurement */
	gpio_clear(sensor_trig);
	usleep(10);
	gpio_set(sensor_trig);
	usleep(10);
	gpio_clear(sensor_trig);
	
	pulse_duration = 0;
	
	/* Wait for value to be available */
	while (pulse_duration == 0) {
		msleep(1);
	}

	/* Convert pulse width in us to distance in mm */
	distance = ((pulse_duration * 10) / (get_main_clock() / (1000*1000)));
	distance = distance / 29;
	/* Send value on serial */

	uprintf(UART0, "dist: %dmm\n", distance);

}

uint8_t cmd_to_color(uint8_t min)
{
	return (cmd[min] - '0') * 100 + (cmd[min + 1] - '0') * 10 + (cmd[min + 2] -'0');

}


#define LED_NORMAL 0
#define LED_FEUX_STOP 1
#define LED_CLIGNOTTEMENT_GAUCHE 2
#define LED_CLIGNOTTEMENT_DROITE 3
#define LED_CUSTOM 4


/****************************************
 * data led gpio 0.0
 * servoMotor data pwm0
 * sensor trig SCL/0_10
 * sensor echo SDA/0_11
 * ************************************* */
int main(void)
{
	uint8_t etat_LEDs = LED_NORMAL;
	uint8_t LED_allume = 0;

	system_init();
	uart_on(UART0, 115200, strip_control);
	adc_on(NULL);
	servo_config(LPC_TIMER_32B1, 0, 0);
	uprintf(0, "Config OK !\n");

	gpio_dir_out(sensor_trig);
	gpio_dir_in(sensor_echo);

	/* Callback on pulse start and end */
	set_gpio_callback(pulse_feedback, &sensor_echo, EDGES_BOTH);

	while (1) {
		if (send == 1) {
			if (cmd[0] == '1') { //set servo 1
				set_servo(channel, (cmd[1] - '0') * 100 + (cmd[2] - '0') * 10 + (cmd[3] -'0'), 0);
			}
			if (cmd[0] == '3') { //set LEDs
				send_color(cmd_to_color(1), cmd_to_color(4), cmd_to_color(7), cmd_to_color(10), cmd_to_color(13), cmd_to_color(15));
				etat_LEDs = LED_CUSTOM;
			}
			if (cmd[0] == 's') {
				etat_LEDs = LED_FEUX_STOP;
			}
			if (cmd[0] == 'g') {
				etat_LEDs = LED_CLIGNOTTEMENT_GAUCHE;
			}
			if (cmd[0] == 'd') {
				etat_LEDs = LED_CLIGNOTTEMENT_DROITE;
			}


			if (cmd[0] == '4') { //get distance from ultrasonic
				send_ultrasonic_sensor_value_on_uart();
			}
			send = 0;
		}

		if (etat_LEDs == LED_NORMAL) {
			send_color(0, 0, 0, 0, 0, 0);
		} else if (etat_LEDs == LED_CLIGNOTTEMENT_GAUCHE) {
			if (LED_allume >= 10) {
		       		send_color(0, 254, 254, 0, 0, 0);
				LED_allume = 0;
			} else {
				send_color(0, 0, 0, 0, 0, 0);
				LED_allume++;
			}
		} else if (etat_LEDs == LED_CLIGNOTTEMENT_DROITE) {
			if (LED_allume >= 10) {
		       		send_color(0, 0, 0, 0, 254, 254);
				LED_allume = 0;
			} else {
				send_color(0, 0, 0, 0, 0, 0);
				LED_allume++;
			}
		} else if (etat_LEDs == LED_FEUX_STOP) {
			send_color(254, 0, 0, 254, 0, 0);
		}
		msleep(100);
			
	}
	return 0;
}



