#include "core/system.h"



/*----------------
 * CONFIG GPIO
 * ---------------*/
#define LPC_AHB_BASE  (0x50000000UL)
#define LPC_GPIO_0_BASE  (LPC_AHB_BASE + 0x00000)
#define LPC_GPIO_1_BASE  (LPC_AHB_BASE + 0x10000)
/* General Purpose Input/Output (GPIO) */
struct lpc_gpio
{
   volatile uint32_t mask;       /* 0x00 : Pin mask, affects data, out, set, clear and invert */
   volatile uint32_t in;         /* 0x04 : Port data Register (R/-) */
   volatile uint32_t out;        /* 0x08 : Port output Register (R/W) */
   volatile uint32_t set;        /* 0x0C : Port output set Register (-/W) */
   volatile uint32_t clear;      /* 0x10 : Port output clear Register (-/W) */
   volatile uint32_t toggle;     /* 0x14 : Port output invert Register (-/W) */
   uint32_t reserved[2];
   volatile uint32_t data_dir;   /* 0x20 : Data direction Register (R/W) */
   /* [.....] */
};
#define LPC_GPIO_0      ((struct lpc_gpio *) LPC_GPIO_0_BASE)
#define LPC_GPIO_1      ((struct lpc_gpio *) LPC_GPIO_1_BASE)

/* The status LED is on GPIO Port 0, pin */
#define CONTROL_PIN 0

struct lpc_gpio* gpio0;


#include "core/system.h"


void code0(uint32_t gpio_bit)
{
		//uprintf(0, "0");

	/* T0H */
	//On met à 1 la pin
	gpio0->set = gpio_bit;
	//On  attend 0.4 µs 
	nop();//20.8 ns * 19
	nop();
	nop();
	nop();
	nop();//->5
	nop();
	nop();
	nop();
	nop();

	/* T1H */
	//On met la pin à 0
	gpio0->clear = gpio_bit;
	//On attend 0.85 µs 
	nop();//20.8 ns * 41
	nop();
	nop();
	nop();
	nop();//->5
	nop();
	nop();
	nop();
	nop();
}

void code1(uint32_t gpio_bit)
{
	//uprintf(0, "1");
	/* T0L */
	//On met à 1 la pin
	gpio0->set = gpio_bit;
	//On attend 0.4 µs
	nop();//20.8 * 22
	nop();
	nop();
	nop();
	nop();//->5
	nop();
	nop();
	nop();
	nop();
	nop();//->10
	nop();
	nop();
	nop();
	nop();
	nop();//->15
	nop();
	
	/* T1L */
	//On met la pin à 0
	gpio0->clear = gpio_bit;

	//On attend 0.8 µs
	nop();//20.8 * 38
	nop();
}

void send_color(uint8_t rouge1, uint8_t vert1, uint8_t bleu1, uint8_t rouge2, uint8_t vert2, uint8_t bleu2)
{
	uint32_t gpio_bit = (1 << CONTROL_PIN);
	int8_t i;
	uint8_t bit;
	lpc_disable_irq();/* On desactive linterruption */

	/* On envoie le vert1 */
	for (i = 7; i >= 0; i--)
	{
		bit = vert1 & (0x01 << i);
		if (bit == 0)
			code0(gpio_bit);
		else
			code1(gpio_bit);
	}
	/* On envoie le rouge1 */
	for (i = 7; i >= 0; i--)
	{
		bit = rouge1 & (0x01 << i);
		if (bit == 0)
			code0(gpio_bit);
		else
			code1(gpio_bit);
	}
	/* On envoie le bleu1 */
	for (i = 7; i >= 0; i--)
	{
		bit = bleu1 & (0x01 << i);
		if (bit == 0)
			code0(gpio_bit);
		else
			code1(gpio_bit);
	}
	/* On envoie le vert2 */
	for (i = 7; i >= 0; i--)
	{
		bit = vert2 & (0x01 << i);
		if (bit == 0)
			code0(gpio_bit);
		else
			code1(gpio_bit);
	}
	/* On envoie le rouge2 */
	for (i = 7; i >= 0; i--)
	{
		bit = rouge2 & (0x01 << i);
		if (bit == 0)
			code0(gpio_bit);
		else
			code1(gpio_bit);
	}
	/* On envoie le bleu2 */
	for (i = 7; i >= 0; i--)
	{
		bit = bleu2 & (0x01 << i);
		if (bit == 0)
			code0(gpio_bit);
		else
			code1(gpio_bit);
	}

	

	//Fin
	gpio0->set = gpio_bit;
	gpio0->clear = gpio_bit;
	
	lpc_enable_irq(); /* On réactive les interruptions */
}

void feux_stop_3s() {
	send_color(254,0,0,254,0,0);
}

void config_pin()
{
	uint32_t gpio_bit = (1 << CONTROL_PIN);
	gpio0 = LPC_GPIO_0;
	gpio0->data_dir |= gpio_bit;

}
