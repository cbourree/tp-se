#include "core/system.h"

void code0(uint32_t gpio_bit);

void code1(uint32_t gpio_bit);

void send_color(uint8_t rouge1, uint8_t vert1, uint8_t bleu1, uint8_t rouge2, uint8_t vert2, uint8_t bleu2);

void config_pin();

void feux_stop_3s();

