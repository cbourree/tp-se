#include <stdint.h>

/*----------------
 * CONFIG GPIO
 * ---------------*/
#define LPC_AHB_BASE  (0x50000000UL)
#define LPC_GPIO_0_BASE  (LPC_AHB_BASE + 0x00000)
#define LPC_GPIO_1_BASE  (LPC_AHB_BASE + 0x10000)
/* General Purpose Input/Output (GPIO) */
struct lpc_gpio
{
   volatile uint32_t mask;       /* 0x00 : Pin mask, affects data, out, set, clear and invert */
   volatile uint32_t in;         /* 0x04 : Port data Register (R/-) */
   volatile uint32_t out;        /* 0x08 : Port output Register (R/W) */
   volatile uint32_t set;        /* 0x0C : Port output set Register (-/W) */
   volatile uint32_t clear;      /* 0x10 : Port output clear Register (-/W) */
   volatile uint32_t toggle;     /* 0x14 : Port output invert Register (-/W) */
   uint32_t reserved[2];
   volatile uint32_t data_dir;   /* 0x20 : Data direction Register (R/W) */
   /* [.....] */
};
#define LPC_GPIO_0      ((struct lpc_gpio *) LPC_GPIO_0_BASE)
#define LPC_GPIO_1      ((struct lpc_gpio *) LPC_GPIO_1_BASE)

/* The status LED is on GPIO Port 0, pin 28 (PIO0_28) and Port 0, pin 29 (PIO0_29) */
#define LED_RED    29
#define LED_GREEN  28

/*----------------
 * CONFIG CHIEN DE GARDE
 * ---------------*/


#define LPC_APB0_BASE         (0x40000000UL)
#define LPC_WDT_BASE           (LPC_APB0_BASE + 0x04000)

/* Watchdog Timer (WDT) */
struct lpc_watchdog
{
    volatile uint32_t mode;          /* 0x000 : Watchdog mode register (R/W) */
    volatile uint32_t timer_const;   /* 0x004 : Watchdog timer constant register (R/W) */
    volatile uint32_t feed_seqence;  /* 0x008 : Watchdog feed sequence register ( /W) */
    volatile const uint32_t timer_value;  /* 0x00C : Watchdog timer value register (R/ ) */
    volatile uint32_t clk_src_sel;   /* 0x010 : Wathdog Clock Source Selection Register (R/W) */
    volatile uint32_t warning_int_compare; /* 0x014 : Watchdog Warning Interrupt compare value. */
    volatile uint32_t window_compare;      /* 0x018 : Watchdog Window compare value. */
};
#define LPC_WDT         ((struct lpc_watchdog *) LPC_WDT_BASE)

/* Stop the watchdog */
void stop_watchdog(void)
{
    struct lpc_watchdog* wdt = LPC_WDT;
    wdt->mode = 0; /* Stop Watchdog timer, and do not block any other config */
    wdt->feed_seqence = 0xAA;
    wdt->feed_seqence = 0x55;
}

/*----------------
 * AUTRES CONFIG
 * ---------------*/
/* Définie une fonction nop en assembleur pour demander au pros de patienter pendant 1 cycle */
#define nop() __asm volatile ("nop")
#define DELAY_SIZE 100
static int memoire[DELAY_SIZE];

void system_init(void)
{
	stop_watchdog();
}

int main(void)
{
   struct lpc_gpio* gpio0 = LPC_GPIO_0;
   int i = 0;
   
   /* Micro-controller init */
   system_init();
   /* Configure the Status Led pins */
   gpio0->data_dir |= (1 << LED_GREEN) | (1 << LED_RED);
   /* Turn Green Led ON */
   gpio0->set = (1 << LED_GREEN);
   gpio0->clear = (1 << LED_RED);

   while (1) {
       volatile uint32_t delay = 0;
       /* Make a backup of current leds state */
       memoire[i++] = gpio0->out;
       if (i >= DELAY_SIZE) {
           i = 0;
       }
       /* Change the led state, but only the green one */
       gpio0->toggle = (1 << LED_GREEN);
       /* Change the red led according to the oldest green state we have */
       if (memoire[i] & (1 << LED_GREEN)) {
           gpio0->set = (1 << LED_RED);
       } else {
           gpio0->clear = (1 << LED_RED);
       }
       while (delay++ < (100 * 1000)) {}
   }
}
